using System.Collections.Generic;
using UnityEngine;

public class Card : IComparer<Card>
{
    public int sign;
    public int suit;

    public int Value { get { return Mathf.Min(sign, 10); } }

    public Card(int sign, int suit)
    {
        this.sign = sign;
        this.suit = suit;
    }
    
    public int Compare(Card x, Card y)
    {
        return x.sign.CompareTo(y.sign);
    }

    public int Compare(Card card)
    {
        return Compare(this, card);
    }
}
