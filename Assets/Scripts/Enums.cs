public enum Suits
{
    Spades,
    Diamond,
    Heart,
    Club
}

public enum CombinationType
{
    Square,
    Straight
}

public enum OrderType
{
    Square,
    Straight,
    Smart
}
