using UnityEngine;

public class GameManager : MonoBehaviour
{
    Player player;
    Deck deck;

    PlayerView playerV;

    void Awake ()
    {
        player = FindObjectOfType<Player>();
        playerV = player.GetComponent<PlayerView>();
        deck = new Deck();
        player.SetCards(deck.TestHand);
    }

    public void OnGenerateNewHand()
    {
        deck = new Deck();
        player.SetCards(deck.GetHand(11));
    }

    public void OnSortPressed(int type)
    {
        playerV.Sort((OrderType)type);
    }
}
