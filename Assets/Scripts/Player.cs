using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] public List<Card> Cards { get;  private set; }

    public Action onNewDeck;

    private List<CardCombination> straightCombinations = new List<CardCombination>();
    private List<CardCombination> squareCombinations = new List<CardCombination>();

    [HideInInspector] public List<int> straightOrder = new List<int>();
    [HideInInspector] public List<int> squareOrder = new List<int>();
    [HideInInspector] public List<int> smartOrder = new List<int>();
    private bool isDirty;

    public void SetCards(List<Card> list)
    {
        Cards = list;
        ArrangeCards();
        onNewDeck?.Invoke();
    }

    /// <summary>
    /// Calculates all different sort types.
    /// </summary>
    public void ArrangeCards()
    {
        isDirty = true;
        // Classify and sort the cards for easier process
        List<int>[] sortedCards = new List<int>[4];
        for (int i = 0; i < sortedCards.Length; i++)
        {
            sortedCards[i] = new List<int>();
        }

        for (int i = 0; i < Cards.Count; i++)
        {
            Card c = Cards[i];
            sortedCards[c.suit].Add(i);
        }

        for (int i = 0; i < sortedCards.Length; i++)
        {
            sortedCards[i].Sort((p1, p2) => Cards[p1].Compare(Cards[p2]));
        }

        GenerateCombinations(sortedCards);
    }

    /// <summary>
    /// Generated possible combinations for given sorted card lists.
    /// </summary>
    /// <param name="sortedCards"></param>
    private void GenerateCombinations(List<int>[] sortedCards)
    {
        //Clear the orders, they are not valid
        straightOrder.Clear();
        squareOrder.Clear();
        smartOrder.Clear();

        foreach (List<int> items in sortedCards)
        {
            straightOrder.AddRange(items);
            squareOrder.AddRange(items);
            smartOrder.AddRange(items);
        }

        squareCombinations.Clear();
        straightCombinations.Clear();

        List<int> signCheckList = new List<int>(Enumerable.Range(1, 13));

        //Find the longest combinations
        for (int sign = 0; sign < sortedCards.Length; sign++)
        {
            for (int j = 0; j < sortedCards[sign].Count; j++)
            {
                List<int> straightIndexes = new List<int>() { sortedCards[sign][j] };
                int straightValue = Cards[straightIndexes[0]].Value;

                // This needs to loop up till card count, to check every square option
                // Out of bounds check need to be done before straight combination calculations
                for (int k = j + 1; k <= sortedCards[sign].Count; j++, k++)
                {
                    int index_1 = sortedCards[sign][j];
                    Card card_1 = Cards[index_1];
                    Card card_2;

                    // Check for square combination
                    List<int> sqrIndex = new List<int>() { index_1 };
                    int sqrValue = card_1.Value;
                    for (int kk = sign + 1; kk < sortedCards.Length; kk++)
                    {
                        if (!signCheckList.Contains(card_1.sign))
                            break;

                        for (int ll = 0; ll < sortedCards[kk].Count; ll++)
                        {
                            card_2 = Cards[sortedCards[kk][ll]];
                            if (card_1.sign == card_2.sign)
                            {
                                sqrIndex.Add(sortedCards[kk][ll]);
                                sqrValue += card_2.Value;
                                break;
                            }
                            else if (card_2.sign > card_1.sign)
                            {
                                break;
                            }
                        }
                    }

                    signCheckList.Remove(card_1.sign);

                    // Add this square combination to combination list, if valid
                    if (sqrIndex.Count >= 3)
                    {
                        squareCombinations.Add(new CardCombination(sqrIndex, sqrValue, CombinationType.Square));
                    }

                    // Out of bounds check
                    if (k == sortedCards[sign].Count)
                        break;

                    int index_2 = sortedCards[sign][k];
                    card_2 = Cards[index_2];

                    // Check for straight combination
                    if (card_2.sign - card_1.sign == 1)
                    {
                        straightIndexes.Add(index_2);
                        straightValue += card_2.Value;
                    }
                    else
                    {
                        break;
                    }
                }

                // Add this straight combination to combination list, if valid
                if (straightIndexes.Count >= 3)
                {
                    straightCombinations.Add(new CardCombination(straightIndexes, straightValue, CombinationType.Straight));
                }
            }
        }
    }

    /// <summary>
    /// Returns a list of sorted card according to order type.
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public List<int> GetCardOrder(OrderType type)
    {
        if (isDirty)
        {
            GenerateOrders();
        }

        switch (type)
        {
            case OrderType.Square:
                return squareOrder;
            case OrderType.Straight:
                return straightOrder;
            case OrderType.Smart:
                return smartOrder;
            default:
                return null;
        }
    }

    /// <summary>
    /// Generates different order types.
    /// </summary>
    private void GenerateOrders()
    {
        foreach (CardCombination comb in squareCombinations)
        {
            foreach (int index in comb.indexes)
            {
                squareOrder.Remove(index);
            }
        }

        foreach (CardCombination comb in straightCombinations)
        {
            foreach (int index in comb.indexes)
            {
                straightOrder.Remove(index);
            }
        }

        foreach (CardCombination comb in squareCombinations)
        {
            squareOrder.InsertRange(0, comb.indexes);
        }

        foreach (CardCombination comb in straightCombinations)
        {
            straightOrder.InsertRange(0, comb.indexes);
        }
        
        GenerateSmartOrder();
        isDirty = false;
    }

    private void GenerateSmartOrder()
    {
        List<CardCombination> smartCombination = new List<CardCombination>();
        smartCombination.AddRange(squareCombinations);
        smartCombination.AddRange(straightCombinations);

        //Calculate relations between different combinations
        for (int i = 0; i < smartCombination.Count; i++)
        {
            for (int j = i + 1; j < smartCombination.Count; j++)
            {
                foreach (int firstIdx in smartCombination[i].indexes)
                {
                    foreach (int secondIdx in smartCombination[j].indexes)
                    {
                        if(firstIdx == secondIdx)
                        {
                            smartCombination[i].AddRelation(new Relation(j, firstIdx, !smartCombination[j].ValidateRemoval(firstIdx)));
                            smartCombination[j].AddRelation(new Relation(i, firstIdx, !smartCombination[i].ValidateRemoval(firstIdx)));
                        }
                    }
                }
            }
        }

        //Get the max values combination with the least relation
        //Untill there is no combination left
        List<CardCombination> result = new List<CardCombination>();
        int count = smartCombination.Count;
        while (count > 0)
        {
            int index = GetHighestCombination(smartCombination);
            result.Add(smartCombination[index]);
            foreach (Relation r in smartCombination[index].relations)
            {
                if (smartCombination[r.otherIdx].RemoveRelation(index, Cards[r.cardIdx].Value) <= 0)
                {
                    smartCombination[r.otherIdx] = null;
                    count--;
                    break;
                }
                // Update the affected relation values
                foreach (Relation rr in smartCombination[r.otherIdx].relations)
                {
                    Relation rel = new Relation(r.otherIdx, rr.cardIdx, !smartCombination[r.otherIdx].ValidateRemoval(rr.cardIdx));
                    smartCombination[rr.otherIdx].UpdateRelation(rel);
                }
            }

            smartCombination[index] = null;
            count--;
        }

        foreach (CardCombination comb in result)
        {
            foreach (int index in comb.indexes)
            {
                smartOrder.Remove(index);
            }
        }

        foreach (CardCombination comb in result)
        {
            smartOrder.InsertRange(0, comb.indexes);
        }
    }

    /// <summary>
    /// Returns highest valued combination from the given list.
    /// </summary>
    /// <param name="list"></param>
    /// <returns></returns>
    private int GetHighestCombination(List<CardCombination> list)
    {
        int relationCount = 100;
        int value = 0;
        int result = -1;
        for (int i = 0; i < list.Count; i++)
        {
            if (list[i] == null)
                continue;

            if(list[i].relationVal < relationCount)
            {
                relationCount = list[i].relationVal;
                value = list[i].value;
                result = i;
            }
            else if(list[i].relationVal == relationCount)
            {
                if(value < list[i].value)
                {
                    relationCount = list[i].relationVal;
                    value = list[i].value;
                    result = i;
                }
            }
        }

        return result;
    }
}
