using System.Collections.Generic;
using UnityEngine;

public static class Utility
{
    private static System.Random rng = new System.Random();

    public static void Shuffle<T>(this List<T> list)
    {
        for (int i = 0; i < list.Count; i++)
        {
            int index = rng.Next(list.Count);
            T temp = list[i];
            list[i] = list[index];
            list[index] = temp;
        }
    }

    public static Object GetCardPrefab()
    {
        return Resources.Load(string.Format("Models/PlayingCards_{0}{1}", 1, ((Suits)0).ToString()));
    }

    public static Mesh GetMesh(int suit, int sign)
    {
        return Resources.Load<Mesh>(string.Format("Models/PlayingCards_{0}{1}", sign, ((Suits)suit).ToString()));
    }
}
