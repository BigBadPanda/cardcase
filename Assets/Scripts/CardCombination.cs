using System.Collections.Generic;

public struct Relation
{
    public int otherIdx;
    public int cardIdx;
    public bool effective;

    public Relation(int otherIdx, int cardIdx, bool effective)
    {
        this.otherIdx = otherIdx;
        this.cardIdx = cardIdx;
        this.effective = effective;
    }
}

public class CardCombination
{
    public List<int> indexes;
    public List<Relation> relations;
    public int value;
    public int relationVal;
    public CombinationType type;

    public CardCombination(List<int> indexes, int value, CombinationType type)
    {
        this.indexes = indexes;
        this.value = value;
        this.type = type;

        relationVal = 0;
        relations = new List<Relation>();
    }

    public int RemoveRelation(int index, int value)
    {
        for (int i = relations.Count - 1; i >= 0; i--)
        {
            if (relations[i].otherIdx == index)
            {
                if (!ValidateRemoval(relations[i].cardIdx))
                    this.value -= 1000;
                else
                    indexes.Remove(relations[i].cardIdx);

                this.value -= value;
                if (relations[i].effective)
                    relationVal--;

                relations.RemoveAt(i);
            }
        }

        return this.value;
    }

    public void AddRelation(Relation relation)
    {
        relations.Add(relation);
        if (relation.effective)
            relationVal++;
    }

    public void UpdateRelation(Relation relation)
    {
        for (int i = 0; i < relations.Count; i++)
        {
            Relation r = relations[i];
            if (relation.otherIdx != r.otherIdx || relation.cardIdx != r.cardIdx)
                continue;

            relations[i] = relation;
            if (relation.effective && !r.effective)
                relationVal++;
        }
    }

    /// <summary>
    /// Checks if the combination is valid after removal of card
    /// </summary>
    /// <param name="cardID"></param>
    /// <returns></returns>
    public bool ValidateRemoval(int cardID)
    {
        if (indexes.Count <= 3)
            return false;

        // If removed object is in the middle return false
        if (type == CombinationType.Straight)
        {
            for (int i = 0; i < indexes.Count; i++)
            {
                if (indexes[i] == cardID && i > 0 && i < indexes.Count - 1)
                    return false;
            }
        }

        return true;
    }
}

