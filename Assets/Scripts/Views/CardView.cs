using UnityEngine;

public class CardView : MonoBehaviour
{
    public int CardIndex { get; private set; }

    private MeshFilter meshF;
    private PlayerView pView;

    public int currentSlot;
    private Vector3 currentPos;

    private void Awake()
    {
        meshF = GetComponent<MeshFilter>();
    }

    public void Set(PlayerView pView)
    {
        this.pView = pView;
    }

    public void UpdateCurrentPos()
    {
        currentPos = transform.localPosition;
    }

    public void UpdateCurrentPos(Vector3 target)
    {
        currentPos = target;
    }

    public void ChangeMesh(Mesh mesh)
    {
        meshF.mesh = mesh;
    }

    public void OnMouseDrag()
    {
        if (!pView.OnDrag(this))
            return;

        transform.localPosition = currentPos + transform.up * 0.1f;
    }

    private void OnMouseEnter()
    {
        transform.localPosition = currentPos + transform.up * 0.1f;
    }

    private void OnMouseExit()
    {
        transform.localPosition = currentPos;
    }
}
