using DG.Tweening;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerView : MonoBehaviour
{
    public float CardWidth = 0.65f;
    private Player player;

    private Camera cam;
    private List<CardView> CardViews;
    private List<int> currentOrder;

    void Start ()
    {
        cam = Camera.main;
        player = GetComponent<Player>();
        player.onNewDeck += PopulateCards;
        PopulateCards();
    }
    
    public void Sort(OrderType type)
    {
        currentOrder = player.GetCardOrder(type);
        UpdateVisual();
    }

    private void PopulateCards()
    {
        if(CardViews == null)
        {
            CardViews = new List<CardView>();
            for (int i = 0; i < player.Cards.Count; i++)
            {
                GameObject go = Instantiate(Utility.GetCardPrefab(), transform) as GameObject;
                go.AddComponent<BoxCollider>().isTrigger = true;
                CardViews.Add(go.AddComponent<CardView>());
            }
        }
        
        for (int i = 0; i < player.Cards.Count; i++)
        {
            Card c = player.Cards[i];
            CardViews[i].Set(this);
            CardViews[i].ChangeMesh(Utility.GetMesh(c.suit, c.sign));
            CardViews[i].transform.position = Vector3.right * 5;
        }

        currentOrder = new List<int>(Enumerable.Range(0, CardViews.Count));
        UpdateVisual(true);
    }

    /// <summary>
    /// Update the visual positions of cards
    /// </summary>
    /// <param name="delayed"></param>
    private void UpdateVisual(bool delayed = false)
    {
        int index = 0;
        int count = CardViews.Count;

        foreach (int idx in currentOrder)
        {
            float slot = index - ((count - 1) * 0.5f);
            float offsetX = slot * CardWidth * 1.1f * 0.5f;
            float offsetY = offsetX * Mathf.Sin(slot * 5f * Mathf.Deg2Rad) * 0.5f;
            Vector3 targetPos = new Vector3(offsetX, -offsetY, -index * 0.01f);

            float delay = delayed ? index * 0.1f : 0;

            CardViews[idx].transform.DOLocalMove(targetPos, 0.2f).SetDelay(delay);
            CardViews[idx].transform.DOLocalRotate(new Vector3(0, 180, slot * 5f), 0.2f).SetDelay(delay);
            CardViews[idx].currentSlot = index;
            CardViews[idx].UpdateCurrentPos(targetPos);
            index++;
        }
    }

    public bool OnDrag(CardView card)
    {
        int slot = GetSlot();

        if (card.currentSlot == slot || slot >= CardViews.Count)
            return false;

        int temp = currentOrder[card.currentSlot];
        currentOrder[card.currentSlot] = currentOrder[slot];
        currentOrder[slot] = temp;
        card.currentSlot = slot;

        UpdateVisual();

        return true;
    }

    /// <summary>
    /// Returns the slot mouse hovering over
    /// </summary>
    /// <returns></returns>
    
    public int GetSlot()
    {
        Vector3 pos = Input.mousePosition;
        pos.z = 3;
        pos = cam.ScreenToWorldPoint(pos);

        int count = CardViews.Count;
        int result = 0;
        for (int i = 0; i < count; i++)
        {
            float slot = i - (count * 0.5f);
            float offsetX = slot * CardWidth * 1.1f * 0.5f;
            if (offsetX < pos.x)
                result++;
        }

        return result;
    }
}
