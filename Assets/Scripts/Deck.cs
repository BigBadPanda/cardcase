using System.Collections.Generic;

public class Deck
{
    private List<Card> cards;

    public List<Card> TestHand = new List<Card> { new Card(1, 2),
                                                  new Card(2, 0),
                                                  new Card(5, 1),
                                                  new Card(4, 2),
                                                  new Card(1, 0),
                                                  new Card(3, 1),
                                                  new Card(4, 3),
                                                  new Card(4, 0),
                                                  new Card(1, 1),
                                                  new Card(3, 0),
                                                  new Card(4, 1)
    };

    public List<Card> Stuff = new List<Card> { new Card(12, 3),
                                                  new Card(12, 0),
                                                  new Card(10, 0),
                                                  new Card(5, 0),
                                                  new Card(5, 2),
                                                  new Card(11, 3),
                                                  new Card(11, 2),
                                                  new Card(8, 1),
                                                  new Card(9, 2),
                                                  new Card(2, 0),
                                                  new Card(12, 2)
    };


    private void CreateDeck()
    {
        cards = new List<Card>();

        for (int i = 0; i < 4; i++)
        {
            for (int j = 1; j <= 13; j++)
            {
                cards.Add(new Card(j, i));
            }
        }

        cards.Shuffle();
    }

    public List<Card> GetHand(int size)
    {
        if (cards == null)
            CreateDeck();

        if (size > cards.Count)
            return null;

        List<Card> result = cards.GetRange(0, size);
        cards.RemoveRange(0, size);
        return result;
    }
}
